public class FirstFile{
    public static void main(String[] args) {
        System.out.println("Hello World from Belarus!!!");
        SuperPupperClass.method01();
    }
}

class SuperPupperClass{

    static void method01(){
        System.out.println("Method 01");
    }

    static void method02(){
        System.out.println("Method 02");
    }

    static void method03(){
        System.out.println("Method 03");
    }

    static void method04(){
        System.out.println("Method 04");
    }

    static void method05(){
        System.out.println("Method 05");
    }

    static void method06(){
        System.out.println("Method 06");
    }

    static void method07(){
        System.out.println("Method 07");
    }

    static void method08(){
        System.out.println("Method 08");
    }

    static void method09(){
        System.out.println("Method 09");
    }

    static void method10(){
        System.out.println("Method 10");
    }

    static void method11(){
        System.out.println("Moon + Moon");
    }

    static void method12(){
        System.out.println("Sun + Sun");
    }

    static void method13(){
        System.out.println("Earth + Earth");
    }

    static void method14(){
        System.out.println("Jupiter + Jupiter");
    }

    static void method15(){
        System.out.println("Mercury");
    }

    static void method16(){
        System.out.println("Venus");
    }

    static void method17(){
        System.out.println("Pluto");
    }

    static void method18(){
        System.out.println("Mars");
    }

    static void method19(){
        System.out.println("Uran");
    }

    static void method20(){
        System.out.println("Saturn");
    }
}